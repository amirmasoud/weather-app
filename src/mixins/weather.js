import axios from 'axios'

export default {
  data () {
    return {
      baseURL: 'http://weather.test/'
    }
  },

  methods: {
    async findWoeid (keyword) {
      return axios.get(this.baseURL + 'weather.php?command=search&keyword=' + keyword)
        .then(result => { return result })
        .catch(error => { return Promise.reject(error) })
    },

    async fetchWeather (woeid) {
      return axios.get(this.baseURL + 'weather.php?command=location&woeid=' + woeid)
        .then(result => { return result })
        .catch(error => { return Promise.reject(error) })
    }
  }
}
