import axios from 'axios'

export default {
  methods: {
    getDayName(dateStr, index, locale = 'us-EN') {
      if (index == 0) {
        return 'Today'
      } else if (index == 1) {
        return 'Tomorrow'
      }
      var castDate = new Date(dateStr)
      return castDate.toLocaleDateString(locale, { weekday: 'long' })
    },

    roundFloat(num) {
      return Number(num.toFixed(2))
    }
  }
}
